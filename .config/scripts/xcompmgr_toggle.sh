#! /usr/bin/bash
msgId="34782932"

# check if process is running ( 0 = off, 1 = on )
STATUS="$(ps -ef | grep -w '[x]compmgr' | wc -l)"
# if off then turn on
if [[ "${STATUS}" == 0 ]]; then
  [[ -z "$1" ]] && dunstify -r $msgId -a "xcompmgr_toggle" -u low "XCompMgr" "On"
  xcompmgr -CcF -t-5 -l-5 -r4.2 -o.55 -I-.02 -O-.02 -D2 /dev/null 2>&1 & disown
# else if on then turn off
elif [[ "${STATUS}" == 1 ]]; then
  [[ -z "$1" ]] && dunstify -r $msgId -a "xcompmgr_toggle" -u low "XCompMgr" "Off"
  killall xcompmgr
fi
